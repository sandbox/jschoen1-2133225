<?php

/**
 * @file
 * admins settings form
 */

function alter_toolbar_admin_form($form, &$form_state) {

  $form['alter_toolbar_css_type'] = array(
    '#type' => 'radios',
    '#options' => array('file' => 'file', 'inline' => 'inline'),
    '#title' => 'grab styles from file, or form?',
    '#default_value' => variable_get('alter_toolbar_css_type', NULL ),
  );

  $css_type = variable_get('alter_toolbar_css_type');

  if($css_type == 'inline')
  {

    $form['instructions'] = array(
      '#type' => 'markup',
      '#markup' => t('Enter a hex color value in the field and press submit to change the main color of the toolbar. <br/>Submitting an empty field will revert the color to it\'s default value.'),
    );

    $form['alter_toolbar_main_color'] = array(
      '#type' => 'textfield',
      '#title' => t('Main toolbar color'),
      '#default_value' => variable_get('alter_toolbar_main_color', NULL),
    );

  }
  else
  {

    $form['instructions'] = array(
      '#type' => 'markup',
      '#markup' => t('<p>OPTIONAL</p><p>Enter the name of the css file you would like to use.</p><p>Leave this field blank to use the module\'s default css file.</p>'),
    );
  
    $form['alter_toolbar_custom_file_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom file name'),
      '#default_value' => variable_get('alter_toolbar_custom_file_name', NULL),
    );

  }

  return system_settings_form($form);

}
