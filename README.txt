alter_toolbar

Created by
	Jay Schoen
Sponsored by
	NumbersUSA

This is a little module that allows you to easily override
the background color of the admin toolbar

1) Enable the module

2) Go to the alter_toolbar configuration page 
	 (admin/config/user-interface/alter_toolbar/settings) 

3) Select either "inline" or "file" depending on how you'd like to load the toolbar CSS

***
[INLINE]
  A)Change the hex value to whatever color you'd like your toolbar to be
  B)Save Configuration

[FILE]
  A) Open "alter_toolbar_custom.css" from within the alter_toolbar directory
  B) Change the hex value to whatever color you'd like your toolbar to be
